<?php

namespace App\Http\Controllers;
use Session;
use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index(){
        $todos = Todo::all();
        return view('todo')->with('todos',$todos);
    }

    public function delete($id){
        $todo = Todo::find($id);
        $todo->delete();
        Session::flash('Success','your todo was deleted!');
        return redirect()->back();
    }

    public function update($id){
        $todo = Todo::find($id);
        return view('update')->with('todo',$todo);
    }

    public function save(Request $request,$id){
        $todo =Todo::find($id);
        $todo->todo = $request->todo;
        $todo->save();
        Session::flash('Success','your todo was updated!');
        return redirect()->route('todo');
    }

    public function complete($id){
        $todo = Todo::find($id);
        $todo->complete = 1;
        $todo->save();
        Session::flash('Success','your todo was add as completed!');
        return redirect()->back();
    }

    public function store(Request $request){
        $todo = new Todo;
        $todo->todo =$request->todo;
        $todo->save();
        Session::flash('Success','your todo was saved!');
        return redirect()->back();
    }
}
