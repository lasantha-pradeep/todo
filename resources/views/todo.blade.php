@extends('layout')

@section('content')
    <form action="/create/todo" method="post">
        {{csrf_field()}}
        <input type="text" name="todo" placeholder="Enter a new todo">
    </form>
    <hr>

    @foreach($todos as $todo)
        {{$todo->todo}}
        <a href="{{route('todo.delete',['id'=>$todo->id])}}">Delete</a>
        <a href="{{route('todo.update',['id'=>$todo->id])}}">Update</a>
        @if(!$todo->complete)
            <a href="{{route('todo.complete',['id'=>$todo->id])}}">Mark as Completed</a>
        @else
            <span>Completed</span>
        @endif
        <hr>
    @endforeach
@endsection