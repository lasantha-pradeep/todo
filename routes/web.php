<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/todo',['uses'=>'TodoController@index', 'as'=>'todo']);

Route::get('/delete/todo/{id}',['uses'=>'TodoController@delete' , 'as'=>'todo.delete']);

Route::get('/update/todo/{id}',['uses'=>'TodoController@update' , 'as'=>'todo.update']);

Route::get('/todo/complete/{id}',['uses'=>'TodoController@complete', 'as'=>'todo.complete']);

Route::post('/todo/save/{id}',['uses'=>'TodoController@save', 'as'=>'todo.save']);

Route::post('/create/todo',['uses'=>'TodoController@store']);